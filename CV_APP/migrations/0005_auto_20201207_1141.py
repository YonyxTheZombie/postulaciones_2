# Generated by Django 2.2.7 on 2020-12-07 14:41

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('CV_APP', '0004_auto_20201207_1141'),
    ]

    operations = [
        migrations.AddField(
            model_name='rubrica_equipomulti',
            name='coordinador',
            field=models.CharField(default=1, max_length=15, verbose_name=''),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='rubrica_equipomulti',
            name='observaciones_c',
            field=models.CharField(default=1, max_length=15, verbose_name=''),
            preserve_default=False,
        ),
    ]
